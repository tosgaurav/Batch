package SpringBatch.domain;

import java.io.Serializable;

/**
 * Created by NSchneier on 9/27/2015.
 */
public class Person implements Serializable {
    private long id;
    private String firstName;
    private String lastName;
    private String status;

    public Person() {}

    public Person (String firstName, String lastName, String status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "First Name: " + this.firstName + ", Last Name: " + this.lastName
                + ", Status: " + this.status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
