package SpringBatch;

import SpringBatch.domain.Person;
import SpringBatch.processor.PersonItemProcessor;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by NSchneier on 9/30/2015.
 */
public class ProcessWriteBatchActor extends UntypedActor {

    private PersonItemProcessor processor = new PersonItemProcessor();

    private static App app = new App();

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof List) {
            List<Person> personList = (ArrayList<Person>)message;
            List<String> queryList = new ArrayList<String>();
            for (Person person : personList) {
                //process the item and write it
                Person processedPerson = processor.process(person);
                String sqlQuery = "INSERT INTO person (firstName, lastName, status) VALUES ('" + processedPerson.getFirstName()
                        + "', '" + processedPerson.getLastName() + "', '" + processedPerson.getStatus() + "')";
                queryList.add(sqlQuery);

            }
            app.write(queryList);
            log.info(new Date().getTime() + "");
        } else {
            unhandled(message);
        }
    }





}
