package SpringBatch.processor;

import SpringBatch.domain.Person;
import org.springframework.batch.item.ItemProcessor;

import java.util.UUID;


/**
 * Created by NSchneier on 9/22/2015.
 */
public class PersonItemProcessor implements ItemProcessor<Person, Person> {

    @Override
    public Person process(Person person) throws Exception {
        final String firstName = person.getFirstName().toUpperCase() + UUID.randomUUID().toString().substring(0, 5);
        final String lastName = person.getLastName().toUpperCase();
        final String status = "PROCESSED";
        Thread.sleep(100);
        return new Person(firstName, lastName, status);
    }
}
