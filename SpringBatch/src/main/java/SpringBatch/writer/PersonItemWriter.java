package SpringBatch.writer;

import SpringBatch.domain.Person;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by NSchneier on 9/22/2015.
 */
public class PersonItemWriter implements ItemWriter<Person> {

    private DataSource dataSource;

    public PersonItemWriter(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    @Override
    public void write(List<? extends Person> list) throws Exception {
        JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<Person>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Person>());
        writer.setSql("INSERT INTO people (firstName, lastName) VALUES (:firstName, :lastName)");
        writer.setDataSource(dataSource);

    }
}
