package SpringBatch.reader;

import SpringBatch.domain.Person;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by NSchneier on 9/22/2015.
 */
public class PersonRowMapper implements RowMapper<Person> {

    private static String firstName = "firstName";
    private static String lastName = "lastName";

    @Override
    public Person mapRow(ResultSet resultSet, int i) throws SQLException {
        Person person = new Person();
        person.setFirstName(resultSet.getString(firstName));
        person.setLastName(resultSet.getString(lastName));
        return person;
    }
}
