package SpringBatch.domain;

import java.io.Serializable;

/**
 * Created by NSchneier on 9/22/2015.
 */
public class Person implements Serializable {
    private long id;
    private String firstName;
    private String lastName;

    public Person() {}

    public Person (String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "First Name: " + this.firstName + ", Last Name: " + this.lastName;
    }
}
