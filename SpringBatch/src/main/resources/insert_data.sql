
use cosmic;

delete from people;
delete from person;

insert into people values ('nneal', 'schneier');
insert into people values ('nnneal', 'schneier');
insert into people values ('nnnneal', 'schneier');
insert into people values ('nnnnneal', 'schneier');
insert into people values ('nnnnnneal', 'schneier');
insert into people values ('nnnnnnneal', 'schneier');
insert into people values ('nnnnnnnneal', 'schneier');
insert into people values ('nnnnnnnnneal', 'schneier');
insert into people values ('nnnnnnnnnneal', 'schneier');
