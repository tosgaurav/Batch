package SpringBatch;

import SpringBatch.actor.PersonRowMapper;
import SpringBatch.domain.Person;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.RoundRobinRouter;

import com.typesafe.config.ConfigFactory;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Logger;

public class App {

    private static Logger logger = Logger.getLogger("App");
    public static Connection connection;
    /**
     * step 1 - Use JDBC Item Reader to perform the  operation
     * step 2 - tell actor to process and write
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        ActorSystem _system = ActorSystem.create("balancing-dispatcher",
                ConfigFactory.load("dispatcher").getConfig("MyDispatcherExample"));

        ActorRef actor = _system.actorOf(new Props(ProcessWriteBatchActor.class)
                .withDispatcher("balancingDispatcher1").withRouter(
                        new RoundRobinRouter(1)));

        connection = getDataSource().getConnection();

        Long startTime = new Date().getTime();
        ItemReader itemReader = new App().itemReader();
        Person person;
        logger.info("Start Time: " + startTime);
        while ((person = (Person) itemReader.read()) != null) {
            actor.tell(person);
        }

        _system.awaitTermination();
        connection.close();
    }

    public static DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/cosmic");
        dataSource.setUsername("root");
        dataSource.setPassword("potatOEz");
        return dataSource;
    }

    public ItemReader<Person> itemReader() throws Exception {
        JdbcCursorItemReader<Person> itemReader = new JdbcCursorItemReader<Person>();
        itemReader.setDataSource(App.getDataSource());
        itemReader.setSql("select * from people where status=''");
        RowMapper<Person> mapper = new PersonRowMapper();
        itemReader.setRowMapper(mapper);
        ExecutionContext executionContext = new ExecutionContext();
        itemReader.open(executionContext);
        return itemReader;
    }

    public void write(String sqlQuery) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(sqlQuery);
        statement.close();
    }


}
