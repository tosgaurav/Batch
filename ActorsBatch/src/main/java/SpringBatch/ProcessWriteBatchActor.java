package SpringBatch;

import SpringBatch.domain.Person;
import SpringBatch.processor.PersonItemProcessor;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.Date;

/**
 * Created by NSchneier on 9/30/2015.
 */
public class ProcessWriteBatchActor extends UntypedActor {

    private PersonItemProcessor processor = new PersonItemProcessor();

    private static App app = new App();

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Person) {
            //process the item and write it
            Person person = (Person) message;
            Person processedPerson = processor.process(person);
            String sqlQuery = "INSERT INTO person (firstName, lastName, status) VALUES ('" + processedPerson.getFirstName()
                    + "', '" + processedPerson.getLastName() + "', '" + processedPerson.getStatus() + "')";
            app.write(sqlQuery);
            log.info(new Date().getTime() + " " + sqlQuery);
        } else {
            unhandled(message);
        }
    }





}
